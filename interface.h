#ifndef INTERFACE_H
#define INTERFACE_H

#include <QString>

class Interface{

public:
  virtual QString name() const = 0;
  virtual void doSomething() const = 0;

};

Q_DECLARE_INTERFACE( Interface, "com.rockybars.pluginapp.interface/1.0")

#endif
