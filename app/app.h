#ifndef APP_H
#define APP_H

#include <QObject>

class App : public QObject{
  Q_OBJECT

public:
  App(QObject * parent = 0);
  ~App(){}

public slots:
  void loadPlugins();
  void loadPlugin(const QString & name);

};



#endif