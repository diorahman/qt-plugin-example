#include <QtCore/QCoreApplication>
#include "app.h"

int main(int argc, char * argv[]){
  QCoreApplication app(argc, argv); 

  App * a = new App(qApp);
  a->loadPlugins();

  return app.exec();
}