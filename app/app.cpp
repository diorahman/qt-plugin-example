#include "app.h"
#include "interface.h"
#include <QDir>
#include <QPluginLoader>
#include <QDebug>

App::App(QObject * parent) : QObject(parent){

}

void App::loadPlugins(){
  QDir path(QDir::currentPath() + "/plugins");

  foreach(QString filename, path.entryList(QDir::Files)){
    loadPlugin(path.absolutePath() + "/" + filename);
  }

}

void App::loadPlugin(const QString & name){
  QPluginLoader loader(name);
  QObject * obj = loader.instance();
  if(obj){
    Interface * plugin = qobject_cast<Interface * >(obj);
    qDebug() << plugin->name();
  }else{
    qDebug() << "Whatt?? " << name;
  }
}