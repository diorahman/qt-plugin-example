TEMPLATE = lib
TARGET = Plugin
CONFIG += plugin release
CONFIG -= app_bundle
version = 1.0.0

INCLUDEPATH = ../ 

target.path = ../app/plugins
INSTALLS += target

HEADERS += plugin.h ../interface.h
SOURCES += plugin.cpp
