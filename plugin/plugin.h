#ifndef PLUGIN_H
#define PLUGIN_H

#include <QObject>
#include <QtPlugin>
#include "interface.h"

class Plugin : public QObject, public Interface{

  Q_OBJECT
  Q_INTERFACES(Interface)

public:
  Plugin(QObject * parent = 0);
  ~Plugin();

  QString name() const;
  void doSomething() const;

};

#endif