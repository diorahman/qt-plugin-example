#include "plugin.h"

#include <QDebug>

Plugin::Plugin(QObject * parent) : QObject(parent){

}

Q_EXPORT_PLUGIN2(Plugin, Plugin)

Plugin::~Plugin(){

}

QString Plugin::name() const{ 
  return "foo";
}

void Plugin::doSomething() const{ 
  qDebug() << "doSomething";
}
